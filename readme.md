Requires :
- Node.js Installed 
- Modules like Restify, BotBuilder, Nodemon (for continous changes reflections)
- BotBuilder Botframework Emulator For Testing Purpose (Optional) 
- ngrok for hosting (Optional) 

Usage :
- Run command node app.js
- Open BotFramework Emulator 
- Insert endpoint as displayed in terminal where u ran node command appending /api/messages
- Test

Download Links :
Node - https://nodejs.org/dist/v8.6.0/node-v8.6.0-x64.msi

BotFramework Emulator - https://github.com/Microsoft/BotFramework-Emulator/releases/download/v3.5.31/botframework-emulator-Setup-3.5.31.exe

For Restify use
npm install restify

For BotBuilder
npm install botbuilder

For Nodemon
npm install nodemon -g //to install it globally

